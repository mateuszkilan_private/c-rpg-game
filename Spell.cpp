#include <iostream>
#include <string>
#include <vector>
#include "Spell.h"
#include "Player.h"

using namespace std;

Spell::Spell(string name, int low, int high, int magicPointsRequired) {
    mName = name;
    mDamageRange.mLow = low;
    mDamageRange.mHigh = high;
    mMagicPointsRequired = magicPointsRequired;
};

void Spell::createNewSpell(Player* player, string name, int low, int high, int magicPointsRequired) {
    Spell newSpell(name, low, high, magicPointsRequired);

    player->insertNewSpell(newSpell);
};
