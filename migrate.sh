FILES=./*
DIRECTORY=./unix_migrated_files
THIS_FILE_NAME=./migrate.sh

echo "======== CREATING NEW DIRECTORY $DIRECTORY========"
mkdir $DIRECTORY

for f in $FILES
do
  # if [ $f == $DIRECTORY ]
  #   then
  #     continue
  # fi

  # if [ $f == $THIS_FILE_NAME ]
  #   then
  #     continue
  # fi

  dos2unix -n $f $DIRECTORY/$f
done
