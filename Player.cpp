#include <iostream>
#include <string>
#include <vector>
#include "Player.h"
#include "Random.h"
#include "Races.h"
#include "CharacterClasses.h"
#include "Spell.h"

using namespace std;

Player::Player() {
    mName = "Default";
    mClassName = "Default";
    mRaceName = "Default";
    mAccuracy = 0;
    mHitPoints = 0;
    mMaxHitPoints = 0;
    mExpPoints = 0;
    mNextLevelExp = 0;
    mLevel = 0;
    mArmor = 0;
    mGold = 0;
    mWeapon.mName = "Default Weapon Name";
    mWeapon.mDamageRange.mLow = 0;
    mWeapon.mDamageRange.mHigh = 0;
    mManaPoints = 0;
    mMaxManaPoints = 0;
};

vector<Spell> Player::getSpellBook() {
    return mSpellBook;
};

void Player::insertNewSpell(Spell newSpell) {
    mSpellBook.push_back(newSpell);
};

void Player::prepareSpellBookWithSpells() {
    Spell::createNewSpell(this, "Fireball", 5, 10, 10);
    Spell::createNewSpell(this, "Magic Missile", 10, 20, 20);
    Spell::createNewSpell(this, "Shield", 30, 40, 40);
};

bool Player::isDead() {
    return mHitPoints <= 0;
};

int Player::getArmor() {
    return mArmor;
};

int Player::getGold() {
    return mGold;
};

void Player::takeDamage (int damage) {
    mHitPoints -= damage;
};

void Player::createClass() {
    cout << "CHARACTER CLASS GENERATION" << endl;
    cout << "==========================" << endl;

    // Input character's name
    cout << "Enter your character's name: ";
    getline(cin, mName);

    // Race selection.
    Races* races = createRaces();
    short int raceChoice = 1;
    cout << "Please select a race number ..." << endl;
    for (int i = 0; i < 4; i++) {
        cout << i + 1 << ") " << races[i].getName() << " ";
    };
    cout << ": ";
    cin >> raceChoice;
    cout << "\n";
    Races chosenRace = races[raceChoice - 1];

    // Character selection.
    CharacterClasses* characterClass = createCharacterClasses();
    short int classChoice = 1;

    cout << "Please select a character class number ..." << endl;

    for (int i = 0; i < 4; i++) {
        cout << i + 1 << ") " << characterClass[i].getStartingClassName() << " ";
    };

    cout << ": ";
    cin >> classChoice;
    cout << "\n";

    mCharacterClass = new CharacterClasses(&characterClass[classChoice - 1]);

    // set character stats based on chosen class and race
    mClassName = mCharacterClass->getStartingClassName();
    mAccuracy = mCharacterClass->getStartingAccuracy() + chosenRace.getAccuracyModifier();
    mHitPoints = mCharacterClass->getStartingMaxHitPoints() + chosenRace.getMaxHitPointsModifier();
    mMaxHitPoints = mCharacterClass->getStartingMaxHitPoints() + chosenRace.getMaxHitPointsModifier();
    mExpPoints = mCharacterClass->getStartingExpPoints();
    mNextLevelExp = mCharacterClass->getStartingExpToNextLvl();
    mLevel = mCharacterClass->getStartingLevel();
    mArmor = mCharacterClass->getStartingArmor() + chosenRace.getArmorModifier();
    mWeapon.mName = mCharacterClass->getStartingWeaponName();
    mWeapon.mDamageRange.mLow = mCharacterClass->getStartingWeaponMinDmg();
    mWeapon.mDamageRange.mHigh = mCharacterClass->getStartingWeaponMaxDmg();
    mGold += chosenRace.getGoldModifier();
    mRaceName = chosenRace.getName();
    mManaPoints = mCharacterClass->getStartingMaxManaPoints() + chosenRace.getMaxManaPointsModifier();
    mMaxManaPoints = mCharacterClass->getStartingMaxManaPoints() + chosenRace.getMaxManaPointsModifier();
    prepareSpellBookWithSpells();

    delete[] races;
    delete[] characterClass;

    characterClass = 0;
    races = 0;
};

Races* Player::createRaces() {
    Races* racesList = new Races[4];
    racesList[0] = Races("dwarf", 5, 10, 5, 0, 5);
    racesList[1] = Races("human", 7, 5, 3, 100, 10);
    racesList[2] = Races("elf", 10, 2, 3, 50, 15);
    racesList[3] = Races("halfling", 8, 1, 2, 300, 20);
    return racesList;
};

CharacterClasses* Player::createCharacterClasses() {
    CharacterClasses* characterClassesList = new CharacterClasses[4];
    // static CharacterClasses characterClassesList[4];

    string classStrings[2];
    int classInts[9];
    int classIntsModifiers[3];

    prepareStringDataForClass(classStrings, "Fighter", "Long Sword");
    prepareIntDataForClass(classInts, 10, 20, 0, 1000, 1, 4, 1, 8, 5);
    prepareIntModDataForClass(classIntsModifiers, 3, 6, 3, 5);
    characterClassesList[0] = CharacterClasses(classStrings, classInts, classIntsModifiers);

    prepareStringDataForClass(classStrings, "Wizard", "Staff");
    prepareIntDataForClass(classInts, 5, 10, 0, 1000, 1, 1, 1, 4, 20);
    prepareIntModDataForClass(classIntsModifiers, 1, 5, 1, 20);
    characterClassesList[1] = CharacterClasses(classStrings, classInts, classIntsModifiers);

    prepareStringDataForClass(classStrings, "Cleric", "Flail");
    prepareIntDataForClass(classInts, 8, 15, 0, 1000, 1, 3, 1, 6, 10);
    prepareIntModDataForClass(classIntsModifiers, 4, 5, 2, 10);
    characterClassesList[2] = CharacterClasses(classStrings, classInts, classIntsModifiers);

    prepareStringDataForClass(classStrings, "Thief", "Short Sword");
    prepareIntDataForClass(classInts, 7, 12, 0, 1000, 1, 2, 1, 6, 5);
    prepareIntModDataForClass(classIntsModifiers, 6, 4, 2, 5);
    characterClassesList[3] = CharacterClasses(classStrings, classInts, classIntsModifiers);    
    
    return characterClassesList;
};

void Player::prepareStringDataForClass(string *classStrings, string startingClassName, string startingWeaponName) {
    classStrings[0] = startingClassName;
    classStrings[1] = startingWeaponName;
};

void Player::prepareIntDataForClass(int *classInts, int accuracy, int maxHitPoints, int expPoints, int expToNextLvl, int level, int armor, int weaponMinDmg, int weaponMaxDmg, int maxManaPoints) {
    classInts[0] = accuracy;
    classInts[1] = maxHitPoints;
    classInts[2] = expPoints;
    classInts[3] = expToNextLvl;
    classInts[4] = level;
    classInts[5] = armor;
    classInts[6] = weaponMinDmg;
    classInts[7] = weaponMaxDmg;
    classInts[8] = maxManaPoints;
};

void Player::prepareIntModDataForClass(int *classIntsModifiers, int accuracyM, int maxHitPointsM, int armorM, int maxManaPointsM) {
    classIntsModifiers[0] = accuracyM;
    classIntsModifiers[1] = maxHitPointsM;
    classIntsModifiers[2] = armorM;
    classIntsModifiers[3] = maxManaPointsM;
};

bool Player::attack(Monster& monster) {
    int selection = 1;
    short int spellChoiceNum = 1;
    Spell* chosenSpell = 0;
    int totalDamage;
    cout << "1) Attack, 2) Cast Spell, 3) Run: ";
    cin >> selection;
    cout << endl;
    
    switch( selection ) {
        case 1:
            cout << "You attack an " << monster.getName()
                 << " with a " << mWeapon.mName << endl;
            
            if ( Random(0, 20) < mAccuracy ) {
                int damage = Random(mWeapon.mDamageRange);
                totalDamage = damage - monster.getArmor();
                if( totalDamage <= 0 ) {
                    cout << "Your attack failed to penetrate "
                         << "the armor." << endl;
                } else {
                    cout << "You attack for " << totalDamage
                         << " damage!" << endl;
                    // Subtract from monster's hitpoints.
                    monster.takeDamage(totalDamage);
                }
            } else {
                cout << "You miss!" << endl;
            }
            cout << endl;
            break;
        case 2:
            cout << "Choose a spell to cast: " << "\n";
            for (int i = 0; i < mSpellBook.size(); i++) {
                cout << i + 1 << ") " << mSpellBook[i].mName << " ";
            }
            
            cin >> spellChoiceNum;
            chosenSpell = &mSpellBook[spellChoiceNum - 1]; 

            if (chosenSpell->mMagicPointsRequired > mManaPoints) {
                cout << "Can't cast spell - not enough mana points, choose different action" << "\n";
                attack(monster);
            }

            totalDamage = Random(chosenSpell->mDamageRange);
            mManaPoints -= chosenSpell->mMagicPointsRequired;
            monster.takeDamage(totalDamage);

            cout << "You hit " << monster.getName() << " for " << totalDamage << " damage.";

            cout << "\n";
            break;
        case 3:
            // 25 % chance of being able to run.
            int roll = Random(1, 4);
            if ( roll == 1 ) {
                cout << "You run away!" << endl;
                return true;//<--Return out of the function.
            } else {
                cout << "You could not escape!" << endl;
                break;
            }
    }

    return false;
};

void Player::levelUp() {
    if (mExpPoints >= mNextLevelExp) {
        cout << "You gained a level!" << endl;
        
        // Increment level.
        mLevel++;

        // Set experience points required for next level.
        mNextLevelExp = mLevel * mLevel * 1000;

        // TODO: this code below gets deleted when classes leveling up will be finally implemented
        // Increase stats randomly.
        mAccuracy     += mCharacterClass->getAccuracyM();
        mMaxHitPoints += mCharacterClass->getMaxHitPointsM();
        mArmor        += mCharacterClass->getArmorM();
        mMaxManaPoints += mCharacterClass->getMaxManaPointsM();

        // Give player full hitpoints when they level up.
        mHitPoints = mMaxHitPoints;
        // Give player full manapoints when they level up.
        mManaPoints = mMaxManaPoints;
    }
};

void Player::rest() {
    cout << "Resting..." << endl;
    mHitPoints = mMaxHitPoints;
    mManaPoints = mMaxManaPoints;
};

void Player::viewStats() {
    cout << "PLAYER STATS" << endl;
    cout << "============" << endl;
    cout << endl;
    cout << "Name = " << mName << endl;
    cout << "Class = " << mClassName << endl;
    cout << "Race = " << mRaceName << endl;
    cout << "Accuracy = " << mAccuracy << endl;
    cout << "Hitpoints = " << mHitPoints << endl;
    cout << "MaxHitpoints = " << mMaxHitPoints << endl;
    cout << "Manapoints = " << mManaPoints << endl;
    cout << "MaxManapoints = " << mMaxManaPoints << endl;
    cout << "XP = " << mExpPoints << endl;
    cout << "XP for Next Lvl = " << mNextLevelExp << endl;
    cout << "Level = " << mLevel << endl;
    cout << "Armor = " << mArmor << endl;
    cout << "Weapon Name = " << mWeapon.mName << endl;
    cout << "Weapon Damage = " << mWeapon.mDamageRange.mLow
         << "-" << mWeapon.mDamageRange.mHigh << endl;
    cout << "Gold = " << mGold << endl;
    cout << endl;
    cout << "END PLAYER STATS" << endl;
    cout << "================" << endl;
    cout << endl;
};

void Player::viewSpellBook() {
    cout << "SPELL BOOK" << endl;
    cout << "============" << endl;
    cout << endl;
    cout << "-------------------" << endl;
    for (short int i = 0; i < mSpellBook.size(); i++) {
        cout << "Name = " << mSpellBook[i].mName << endl;
        cout << "Damage = " << mSpellBook[i].mDamageRange.mLow << " - " << mSpellBook[i].mDamageRange.mHigh << endl;
        cout << "Cost = " << mSpellBook[i].mMagicPointsRequired << endl;
        cout << "-------------------" << endl;
    }
    cout << endl;
    cout << "END SPELL BOOK" << endl;
    cout << "================" << endl;
    cout << endl;
}

void Player::victory(int xp, int goldReward) {
    cout << "You won the battle!" << endl;
    cout << "You win " << xp << " experience points!" << endl;
    cout << "And you also get additional " << goldReward << " gold" << endl << endl;
    mExpPoints += xp;
    mGold += goldReward;
};

void Player::gameover() {
    cout << "You died in battle..." << endl;
    cout << endl;
    cout << "==================================" << endl;
    cout << "GAME OVER!" << endl;
    cout << "==================================" << endl;
    cout << "Press 'q' to quit: ";
    char q = 'q';
    cin >> q;
    cout << endl;
};

void Player::displayHitPoints() {
    cout << mName << "'s hitpoints = " << mHitPoints << endl;
};
