#ifndef RACES_H
    #define RACES_H
    #include <string>
    using namespace std;
    
    class Races {
        public:
            Races();
            Races(string name, int accuracyM, int maxHitPointsM, int armorM, int goldM, int maxManaPointsM);
            int getAccuracyModifier();
            int getMaxHitPointsModifier();
            int getArmorModifier();
            int getGoldModifier();
            int getMaxManaPointsModifier();
            string getName();
        private:
            string mName;
            int mAccuracyModifier;
            int mMaxHitPointsModifier;
            int mArmorModifier;
            int mGoldModifier;
            int mMaxManaPointsModifier;
    };
#endif