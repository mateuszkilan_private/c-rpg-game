#ifndef CHARACTER_CLASSES_H
    #define CHARACTER_CLASSES_H
    #include <string>
    using namespace std;

    class CharacterClasses {
        public:
            CharacterClasses();
            CharacterClasses(const CharacterClasses* instanceToCopy);
            CharacterClasses(string* classAndWeaponNames, int* startingStats, int* modifierStats);
            int getAccuracyM();
            int getMaxHitPointsM();
            int getMaxManaPointsM();
            int getArmorM();
            string getStartingClassName();
            int getStartingAccuracy();
            int getStartingMaxHitPoints();
            int getStartingExpPoints();
            int getStartingLevel();
            int getStartingArmor();
            string getStartingWeaponName();
            int getStartingWeaponMinDmg();
            int getStartingWeaponMaxDmg();
            int getStartingExpToNextLvl();
            int getStartingMaxManaPoints();
            void logAll();
        private:
            CharacterClasses* operator =(const CharacterClasses* instanceToCopy);
            int mAccuracyM;
            int mMaxHitPointsM;
            int mMaxManaPointsM;
            int mArmorM;
            string mStartingClassName;
            int mStartingAccuracy;
            int mStartingMaxHitPoints;
            int mStartingExpPoints;
            int mStartingLevel;
            int mStartingArmor;
            string mStartingWeaponName;
            int mStartingWeaponMinDmg;
            int mStartingWeaponMaxDmg;
            int mStartingExpToNextLvl;
            int mStartingMaxManaPoints;
    };
#endif