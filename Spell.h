#ifndef SPELL_H
    #define SPELL_H
    #include <string>
    #include <vector>
    #include "Range.h"

    using namespace std;

    class Player;
    struct Spell {
        Spell(string name, int low, int high, int magicPointsRequired);
        string mName;
        Range mDamageRange;
        int mMagicPointsRequired;
        static void createNewSpell(Player* player, string name, int low, int high, int magicPointsRequired);
    };
#endif