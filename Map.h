#ifndef MAP_H
    #define MAP_H
    #include "Weapon.h"
    #include "Monster.h"
    #include "Random.h"
    #include <string>

    class Map {
        public:
            Map();
            int getPlayerXPos();
            int getPlayerYPos();
            void movePlayer();

            Monster* checkRandomEncounter(int roll = Random(0, 20));
            Monster* checkRandomEncounterDuringRest();
            void printPlayerPos();
        private:
            // Data members.
            int mPlayerXPos;
            int mPlayerYPos;
    };
#endif