#include <iostream>
#include "Map.h"
#include "Player.h"
#include <cstdlib>
#include <ctime>

using namespace std;

bool startEncounter(Map gameMap, Player mainPlayer, Monster* monster) {
    while ( true ) {
        // Display hitpoints.
        mainPlayer.displayHitPoints();
        monster->displayHitPoints();
        cout << endl;
        // Player's turn to attack first.
        bool runAway = mainPlayer.attack(*monster);
        if( runAway )
            break;
        if ( monster->isDead() ) {
            mainPlayer.victory(monster->getXPReward(), monster->getGoldReward());
            mainPlayer.levelUp();
            break;
        }

        monster->attack(mainPlayer);
        if ( mainPlayer.isDead() ) {
            mainPlayer.gameover();
            return true;
        }
    }
    // The pointer to a monster returned from
    // checkRandomEncounter was allocated with
    // 'new', so we must delete it to avoid
    // memory leaks.
    delete monster;
    monster = 0;
    return false;
};

int main() {
    srand(static_cast<unsigned int>(time(0)));
    Map gameMap;
    Player mainPlayer;
    mainPlayer.createClass();

    // Begin adventure.
    bool done = false;
    while ( !done ) {
        // Each loop cycly we output the player position and
        // a selection menu.
        gameMap.printPlayerPos();

        int selection = 1;
        cout << "1) Move, 2) Rest, 3) View Stats, 4) View Spell Book 5) Quit Game: ";
        cin >> selection;
        Monster* monster = 0;
        switch( selection ) {
            case 1:
                // Move the player.
                gameMap.movePlayer();
                gameMap.getPlayerXPos() == 2 && gameMap.getPlayerYPos() == 4 && gameMap.visitStore(mainPlayer);
                // Check for a random encounter. This function
                // returns a null pointer if no monsters are
                // encountered.
                monster = gameMap.checkRandomEncounter();
                // 'monster' not null, run combat simulation.
                if ( monster != 0 ) {
                    done = startEncounter(gameMap, mainPlayer, monster);
                }
                break;
            case 2:
                monster = gameMap.checkRandomEncounterDuringRest();

                if ( monster != 0 ) {
                    done = startEncounter(gameMap, mainPlayer, monster);
                }
                mainPlayer.rest();
                break;
            case 3:
                mainPlayer.viewStats();
                break;
            case 4:
                mainPlayer.viewSpellBook();
                break;
            case 5:
                done = true;
                break;
        }// End Switch Statement
    }// End While Statement
};
