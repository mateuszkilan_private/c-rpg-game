#ifndef PLAYER_H
#define PLAYER_H

#include "Weapon.h"
#include "Monster.h"
#include "Races.h"
#include "Spell.h"
#include <string>
#include <vector>

using namespace std;

class CharacterClasses;
class Player {
    public:
        Player();

        bool isDead();

        string getName();
        int    getArmor();

        void takeDamage(int damage);

        void createClass();
        bool attack(Monster& monster);
        void levelUp();
        void rest();
        void viewStats();
        void victory(int xp, int goldReward);
        void gameover();
        void displayHitPoints();
        int  getGold();
        vector<Spell> getSpellBook();
        void insertNewSpell(Spell newSpell);
        void viewSpellBook();
    private:
        Races* createRaces();
        CharacterClasses* createCharacterClasses();
        void prepareStringDataForClass(string *classStrings, string startingClassName, string startingWeaponName);
        void prepareIntDataForClass(int *classInts, int accuracy, int maxHitPoints, int expPoints, int expToNextLvl, int level, int armor, int weaponMinDmg, int weaponMaxDmg, int maxManaPoints);
        void prepareIntModDataForClass(int *classFloats, int accuracyM, int maxHitPointsM, int armorM, int maxManaPointsM);
        void prepareSpellBookWithSpells();
        CharacterClasses* mCharacterClass;
        string mName;
        string mClassName;
        string mRaceName;
        int    mAccuracy;
        int    mHitPoints;
        int    mMaxHitPoints;
        int    mExpPoints;
        int    mNextLevelExp;
        int    mLevel;
        int    mArmor;
        int    mGold;
        int    mManaPoints;
        int    mMaxManaPoints;
        Weapon mWeapon;
        vector<Spell> mSpellBook;
};
#endif