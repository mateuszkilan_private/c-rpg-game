#include <iostream>
#include <string>
#include "CharacterClasses.h"

using namespace std;

CharacterClasses::CharacterClasses() {};

CharacterClasses::CharacterClasses(const CharacterClasses* instanceToCopy) {
    *this = instanceToCopy;
};

CharacterClasses::CharacterClasses(string* classAndWeaponNames, int* startingStats, int* modifierStats) {
    mStartingClassName = classAndWeaponNames[0];
    mStartingWeaponName = classAndWeaponNames[1];

    mStartingAccuracy = startingStats[0];
    mStartingMaxHitPoints = startingStats[1];
    mStartingExpPoints = startingStats[2];
    mStartingExpToNextLvl = startingStats[3];
    mStartingLevel = startingStats[4];
    mStartingArmor = startingStats[5];
    mStartingWeaponMinDmg = startingStats[6];
    mStartingWeaponMaxDmg = startingStats[7];
    mStartingMaxManaPoints = startingStats[8];

    mAccuracyM = modifierStats[0];
    mMaxHitPointsM = modifierStats[1];
    mArmorM = modifierStats[2];
    mMaxManaPointsM = modifierStats[3];
};

CharacterClasses* CharacterClasses::operator =(const CharacterClasses* instanceToCopy) {
    if (this == instanceToCopy) return this;

    this->mStartingClassName = instanceToCopy->mStartingClassName;
    this->mStartingWeaponName = instanceToCopy->mStartingWeaponName;
    this->mStartingAccuracy = instanceToCopy->mStartingAccuracy;
    this->mStartingMaxHitPoints = instanceToCopy->mStartingMaxHitPoints;
    this->mStartingExpPoints = instanceToCopy->mStartingExpPoints;
    this->mStartingExpToNextLvl = instanceToCopy->mStartingExpToNextLvl;
    this->mStartingLevel = instanceToCopy->mStartingLevel;
    this->mStartingArmor = instanceToCopy->mStartingArmor;
    this->mStartingWeaponMinDmg = instanceToCopy->mStartingWeaponMinDmg;
    this->mStartingWeaponMaxDmg = instanceToCopy->mStartingWeaponMaxDmg;
    this->mStartingMaxManaPoints = instanceToCopy->mStartingMaxManaPoints;

    this->mAccuracyM = instanceToCopy->mAccuracyM;
    this->mMaxHitPointsM = instanceToCopy->mMaxHitPointsM;
    this->mArmorM = instanceToCopy->mArmorM;
    this->mMaxManaPointsM = instanceToCopy->mMaxManaPointsM;

    return this;
};

void CharacterClasses::logAll() {
    cout << mStartingClassName << endl;
    cout << mStartingWeaponName << endl;
    cout << mStartingAccuracy << endl;
    cout << mStartingMaxHitPoints << endl;
    cout << mStartingExpPoints << endl;
    cout << mStartingExpToNextLvl << endl;
    cout << mStartingLevel << endl;
    cout << mStartingArmor << endl;
    cout << mStartingWeaponMinDmg << endl;
    cout << mStartingWeaponMaxDmg << endl;
    cout << mAccuracyM << endl;
    cout << mMaxHitPointsM << endl;
    cout << mArmorM << endl;
};

int CharacterClasses::getAccuracyM() {
    return mAccuracyM;
};

int CharacterClasses::getMaxHitPointsM() {
    return mMaxHitPointsM;
};

int CharacterClasses::getArmorM() {
    return mArmorM;
};

int CharacterClasses::getMaxManaPointsM() {
    return mMaxManaPointsM;
}

int CharacterClasses::getStartingExpToNextLvl() {
    return mStartingExpToNextLvl;
}

int CharacterClasses::getStartingMaxManaPoints() {
    return mStartingMaxManaPoints;
}

string CharacterClasses::getStartingClassName() {
    return mStartingClassName;
};

int CharacterClasses::getStartingAccuracy() {
    return mStartingAccuracy;
};

int CharacterClasses::getStartingMaxHitPoints() {
    return mStartingMaxHitPoints;
};

int CharacterClasses::getStartingExpPoints() {
    return mStartingExpPoints;
};

int CharacterClasses::getStartingLevel() {
    return mStartingLevel;
};

int CharacterClasses::getStartingArmor() {
    return mStartingArmor;
};

string CharacterClasses::getStartingWeaponName() {
    return mStartingWeaponName;
};

int CharacterClasses::getStartingWeaponMinDmg() {
    return mStartingWeaponMinDmg;
};

int CharacterClasses::getStartingWeaponMaxDmg() {
    return mStartingWeaponMaxDmg;
};
