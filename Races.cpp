#include <iostream>
#include <string>
#include "Races.h"

using namespace std;

Races::Races() {
    mAccuracyModifier = 0;
    mMaxHitPointsModifier = 0;
    mArmorModifier = 0;
    mGoldModifier = 0;
    mName = "default";
}

Races::Races(string name, int accuracyM, int maxHitPointsM, int armorM, int goldM, int maxManaPointsM) {
    mAccuracyModifier = accuracyM;
    mMaxHitPointsModifier = maxHitPointsM;
    mArmorModifier = armorM;
    mGoldModifier = goldM;
    mName = name;
    mMaxManaPointsModifier = maxManaPointsM;
}

int Races::getAccuracyModifier() {
    return mAccuracyModifier;
};

int Races::getMaxHitPointsModifier() {
    return mMaxHitPointsModifier;
};

int Races::getMaxManaPointsModifier() {
    return mMaxManaPointsModifier;
}

int Races::getArmorModifier() {
    return mArmorModifier;
};

int Races::getGoldModifier() {
    return mGoldModifier;
}

string Races::getName() {
    return mName;
}
